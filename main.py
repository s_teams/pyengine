
import pygame
#import time #for timings
import os #directories
import random

from propertyContainer import PropContainer
from math import sqrt

from Levels import AllLevels

random.seed(23)
clock = pygame.time.Clock()
PGTicks = pygame.time.get_ticks



##
##import os
##dirname = os.path.dirname(os.path.abspath(__file__))+'/'
CS_SPRITEPARAM_STATE = 'State'

CS_SPRITESTATE_IDLE = 'Idle'
CS_SPRITESTATE_FLAG = 'Flag'
CS_SPRITESTATE_LEAK = 'Leak'
CS_SPRITESTATE_ULEAK = 'ULeak'
CS_SPRITESTATE_SRAIRS = 'stairs'
CS_SPRITESTATE_COLUMN = 'Column'

CS_SPRITESTATE_SPIKES = 'Spikes'
CS_SPRITESTATE_SPIKESIDLE = 'SpikesIdle'


class Drawable(PropContainer):
  def Surf(self):
    return None;



class TheSpriteFrameSet(Drawable):

  def __init__(self, **params):
    self.Frames = [] #dict()
    super().__init__(**params)
    #self.Owner = InSprite
    #self['stTime'] = PGTicks()
    #self['frameIndex'] = 0
    #self.OnPropChange = self.CheckPropChange
    
  def Surf(self):
    return self.Calc(self)

  def Calc(self, Params):
    if 'frameIndex' in Params:
      fIdx = Params['frameIndex']
      if (fIdx >= 0) and (fIdx < len(self.Frames)):
        return self.Frames[ fIdx ]
      else:
        return None
        
    elif (len(self.Frames) > 0) and ('Delay' in Params):
      tIdx = PGTicks() - Params['stTime']
      tIdx = tIdx / Params['Delay']
      if 'Shift' in Params:
        tIdx = tIdx + Params['Shift']
      tIdx = int(tIdx) % len(self.Frames)
      fIdx = tIdx
      if (fIdx >=0) and (fIdx<len(self.Frames)):
        return self.Frames[ fIdx ]
      else:
        return None
  
  def Load(self, aDir, aFileNames = None, aFileName = ''):
    if not (aFileName == ''):
      aFrm = pygame.image.load(os.path.join(aDir, aFileName) )
      if 'Size' in self:
        aFrm = pygame.transform.scale(aFrm, (self['Size'], self['Size']))
      self.Frames.append( aFrm.convert_alpha() );
    if not (aFileNames is None):
      for fn in aFileNames:
        self.Load(aDir, aFileName = fn)



class TheSprite(Drawable):

  def __init__(self, **params):
    self.Pictures = dict()
    super().__init__()
    self.OnPropChange = self.CheckPropChange
    self.Setup(**params)
    self.Setup(State=CS_SPRITESTATE_IDLE)

  def currentPictures(self, State = None, Color = None, Direction = None):
    fIdx = self.PicIndex(State, Color, Direction)
    if not (fIdx in self.Pictures):
      self.Pictures[ fIdx ] = TheSpriteFrameSet(Size = self['Size'], Name = fIdx)
      if not ('Colors' in self): self['Colors'] = []
      if (not(Color in self['Colors'])) and (Color is not None):
        self['Colors'].append(Color)
    return self.Pictures[ fIdx ]
  
  def PicIndex(self, State = None, Color = None, Direction = None):
    fColor = None
    if not (Color is None):      
      fColor = Color
    elif 'Color' in self:
      fColor = self['Color']
      
    if not(State is None):
      fState = State
    else:
      fState = self[CS_SPRITEPARAM_STATE]

    if not (Direction is None):
      fDir = Direction
    elif 'Direction' in self:
      fDir = self['Direction']
    else:
      fDir = None
      
    return str(fState)+':'+str(fColor)+':'+str(fDir)
  
  def Surf(self, aState = None):
    return self.currentPictures(aState = aState).Surf()

  def InitClone(self, Clone):
    if 'Colors' in self:
      Clone['Colors'] = self['Colors']
    if 'Name' in self:
      Clone['ParentName'] = self['Name']

  def CheckPropChange(self, aName, aValue):
    if (aName == 'Color'):
      if not ('Colors' in self): self['Colors'] = []
      if not(aValue in self['Colors']): self['Colors'].append(aValue)
    
    #if (aName == CS_SPRITEPARAM_STATE):
    #  fIdx = self.PicIndex(aState = aValue)
    #  if (not (fIdx in self.Pictures)):
    #    self.Pictures[ fIdx ] = TheSpriteFrameSet(self, Size = self['Size'], Name=aValue)
    #if (aName == 'Color'):
    #  fIdx = self.PicIndex(aColor = aValue)
    #  if (not (fIdx in self.Pictures)):
    #    self.Pictures[ fIdx ] = TheSpriteFrameSet(self, Size = self['Size'], Name=aValue)
    #super().CheckPropChange(aName, aValue)
    return True

def DoAction_NextColor(Object, Subject = None, ActionParams = None):
  if ('Colors' in Object) and ('Color' in Object):
    fIdx = Object['Colors'].index( Object['Color'] )
    fIdx = (fIdx +1) % len(Object['Colors'])
    Object['Color'] = Object['Colors'][fIdx]
##    if 'Link' in Prop:
##      Prop['Link']['Color'] = Prop['Color']

class SpriteClone(Drawable):
  
  def __init__(self, Sprite, **Params):
    self.Sprite = Sprite
    super().__init__()
    self['State'] = CS_SPRITESTATE_IDLE
    #self['frameIndex'] = 0
    self.Setup(**Params)
    self.Sprite.InitClone(self)
    #self.CacheSurf = self.Sprite.currentPictures(self['State'], self['Color'])

    if 'Rand' in self:
      if 'Color' in self['Rand']:
        clrIdx = random.randint(0, len(self['Colors']) -1 )
        self['Color'] = self['Colors'][clrIdx]
      else:
        if 'Color' in self:
          fFrames = self.Sprite.currentPictures(self['State'], self['Color']).Frames
        else:
          fFrames = self.Sprite.currentPictures(self['State']).Frames
        if len(fFrames) > 1:
          self['frameIndex'] = random.randint(0, len(fFrames) -1 )

    if 'Color' in self:
      fFrames = self.Sprite.currentPictures(self['State'], self['Color']).Frames
    else:
      fFrames = self.Sprite.currentPictures(self['State']).Frames
    if len(fFrames) < 1:
      if (not 'Color' in self) and (len(self['Colors']) > 0):
        self['Color'] = self['Colors'][0]
    if (not 'Delay' in self) and (not 'frameIndex' in self):
      self['frameIndex'] = 0
      
    self.OnPropChanged = self.CheckPropChanged
    self.CheckPropChanged('Color', None)
    
    if ('Print' in self) and ('Create' in self['Print']):
      print(repr(self))
    
  def Surf(self):
    return self.CacheSurf.Calc(self)

  def CheckPropChanged(self, aName, aValue):
    if aName in ['Color', 'State']:
      if 'Color' in self:
        self.CacheSurf = self.Sprite.currentPictures(self['State'], self['Color'])
      else:
        self.CacheSurf = self.Sprite.currentPictures(self['State'])
    #return True

  def Action(self, ActionID, Subject, ActionParams):
    if ('Action' in self) and (ActionID in self['Action']):
      self['Action'][ActionID](self, Subject, ActionParams)
    if ('Print' in self) and ('Action' in self['Print']):
      print(repr(self))


##########################################################
##########################################################
##########################################################
##########################################################
##########################################################


def initGraph(screen_width = 400, screen_height = 400):
  pygame.init()
  pygame.display.init()
  #pygame.mouse.set_visible(False)
  
  #Resolution is ignored on Android
  #screen=pygame.display.set_mode(size=[screen_width,screen_height], flags=pygame.NOFRAME)  flags=pygame.FULLSCREEN
  screen = pygame.display.set_mode(size=[screen_width, screen_height]) #size=[screen_width, screen_height]

initGraph(screen_height=600, screen_width=700)


##########################################################
##########################################################
##########################################################
##########################################################
##########################################################

dirname = os.path.dirname(os.path.abspath(__file__)) + '\\'
dirResources = dirname + 'res'
ThSize = 24


hole = TheSprite(Size=ThSize, Name='Hole')
hole.currentPictures().Load(dirResources, aFileName ='hole.png');

brick = TheSprite(Size=ThSize, Name='Brick')
brick.currentPictures().Load(dirResources, aFileName ='wall_mid.png');
brick.currentPictures().Load(dirResources, aFileName ='wall_hole_1.png');
brick.currentPictures().Load(dirResources, aFileName ='wall_hole_2.png');

brick[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_LEAK
brick.currentPictures(Color='Red').Load(dirResources, aFileNames=['wall_fountain_mid_red_anim_f0.png', 'wall_fountain_mid_red_anim_f1.png', 'wall_fountain_mid_red_anim_f2.png'])
brick.currentPictures(Color='Blue').Load(dirResources, aFileNames=['wall_fountain_mid_blue_anim_f0.png', 'wall_fountain_mid_blue_anim_f1.png', 'wall_fountain_mid_blue_anim_f2.png'])
brick.currentPictures(Color='Green').Load(dirResources, aFileNames=['wall_goo.png'])

brick[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_ULEAK
brick.currentPictures(Color='Red').Load(dirResources, aFileNames=['wall_fountain_basin_red_anim_f0.png', 'wall_fountain_basin_red_anim_f1.png', 'wall_fountain_basin_red_anim_f2.png'])
brick.currentPictures(Color='Blue').Load(dirResources, aFileNames=['wall_fountain_basin_blue_anim_f0.png', 'wall_fountain_basin_blue_anim_f1.png', 'wall_fountain_basin_blue_anim_f2.png'])
brick.currentPictures(Color='Green').Load(dirResources, aFileNames=['wall_goo_base.png'])


brick[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_COLUMN
brick.currentPictures().Load(dirResources, aFileNames=['column_mid.png'])
brick.currentPictures().Load(dirResources, aFileNames=['column_top.png'])
brick.currentPictures().Load(dirResources, aFileNames=['coulmn_base.png'])



Flag = TheSprite(Size=ThSize, Name='Flag')
Flag.currentPictures(Color = 'Blue').Load(dirResources, aFileName ='wall_banner_blue.png');
Flag.currentPictures(Color = 'Green').Load(dirResources, aFileName ='wall_banner_green.png');
Flag.currentPictures(Color = 'Red').Load(dirResources, aFileName ='wall_banner_red.png');
Flag.currentPictures(Color = 'Yellow').Load(dirResources, aFileName ='wall_banner_yellow.png');


floor = TheSprite(Size=ThSize, Name='floor')
floor.currentPictures().Load(dirResources, aFileName ='floor_1.png')

floor[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_SPIKESIDLE
floor.currentPictures().Load(dirResources, aFileNames=['floor_spikes_anim_f0.png'])

floor[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_SRAIRS
floor.currentPictures().Load(dirResources, aFileNames=['floor_ladder.png'])


floor[CS_SPRITEPARAM_STATE] = CS_SPRITESTATE_SPIKES
floor.currentPictures().Load(dirResources, aFileNames=['floor_spikes_anim_f0.png',
                                                       'floor_spikes_anim_f1.png',
                                                       'floor_spikes_anim_f2.png',
                                                       'floor_spikes_anim_f3.png',
                                                       'floor_spikes_anim_f2.png',
                                                       'floor_spikes_anim_f1.png',
                                                      ])


_MapTiles = {'w'  : SpriteClone(brick, State=CS_SPRITESTATE_IDLE),
             'o'  : SpriteClone(hole, State=CS_SPRITESTATE_IDLE),
             'H'  : SpriteClone(floor, State=CS_SPRITESTATE_SRAIRS),
             'N'  : SpriteClone(brick, State=CS_SPRITESTATE_COLUMN),

             ' '  : SpriteClone(floor, State=CS_SPRITESTATE_IDLE),
             #'v1' : SpriteClone(floor, Delay=1000/20, State=CS_SPRITESTATE_SPIKES),
             'v2' : SpriteClone(floor, Delay=1000/5, State=CS_SPRITESTATE_SPIKES),
             
             'v3' : SpriteClone(floor, Delay=1000/3, State=CS_SPRITESTATE_SPIKES, Shift=1),
             'v4' : SpriteClone(floor, Delay=1000/3, State=CS_SPRITESTATE_SPIKES, Shift=2),
             'v5' : SpriteClone(floor, Delay=1000/3, State=CS_SPRITESTATE_SPIKES, Shift=3),
             'v6' : SpriteClone(floor, Delay=1000/3, State=CS_SPRITESTATE_SPIKES, Shift=4),
            
             'r' : SpriteClone(brick, Color='Red', State = CS_SPRITESTATE_ULEAK, Delay=1000/2, Name='r'),
             'R' : SpriteClone(brick, Color='Red', State = CS_SPRITESTATE_LEAK, Delay=1000/2, Action={'Click':DoAction_NextColor}, Name='R'),
           
             'b'  : SpriteClone(brick, Color='Blue', State = CS_SPRITESTATE_ULEAK, Delay=1000/3),
             'B'  : SpriteClone(brick, Color='Blue', State = CS_SPRITESTATE_LEAK, Delay=1000/3),
            
             'G'  : SpriteClone(brick, Color='Green', State = CS_SPRITESTATE_LEAK),
             'g'  : SpriteClone(brick, Color='Green', State = CS_SPRITESTATE_ULEAK),

             'FR' : SpriteClone(Flag, Color='Red', Name='FR'),
             'FG' : SpriteClone(Flag, Color='Green', Name='FG'),
             'FB' : SpriteClone(Flag, Color='Blue', Action={'Click':DoAction_NextColor}, Name='FB'),
             'FY' : SpriteClone(Flag, Color='Yellow', sRand=['Color'], Name='FY'),
            }


def MapTiles(aPnt):
  if aPnt is None:         return None

  if aPnt == ' ':          return SpriteClone(floor, State=CS_SPRITESTATE_IDLE)
  if aPnt == 'w':          return SpriteClone(brick, State=CS_SPRITESTATE_IDLE)  
  if aPnt in ['W']:        return SpriteClone(brick, Rand=[], Name='W#')
  
  if aPnt in ['R1', 'R2']: return SpriteClone(brick, Color='Red', State = CS_SPRITESTATE_LEAK, Delay=1000/2, Action={'Click':DoAction_NextColor}, Print=['Action'], Name='R#')

  if aPnt in ['F#']:       return SpriteClone(Flag, Rand=['Color'], Name='F#')
  if aPnt in ['FR']:       return SpriteClone(Flag, Color='Red', Name='FR')
  if aPnt in ['FG']:       return SpriteClone(Flag, Color='Green', Name='FG')
  if aPnt in ['FB']:       return SpriteClone(Flag, Color='Blue', Action={'Click': DoAction_NextColor}, Print=['Action'], Name='FB')
  if aPnt in ['FY']:       return SpriteClone(Flag, Color='Yellow', sRand=['Color'], Name='FY')
  if aPnt in ['v1']:       return SpriteClone(floor, Delay=1000/10, State=CS_SPRITESTATE_SPIKES, Shift = random.randint(0, 4) )

  if aPnt in _MapTiles:
    return _MapTiles[aPnt]
  


def RenderMap(aLevel):
  RendedMap = {}
  
  MaxS = 0
  fmap = aLevel['Map']
  for y in range(len(fmap)):
    if MaxS < len(fmap[y]):
      MaxS = len(fmap[y])
    for x in range(len(fmap[y])):
      tile = fmap[y][x]
      Sprt = MapTiles(tile)
      RendedMap[x, y] = Sprt
  RendedMap['Size'] = (len(fmap), MaxS)

  fLights = []
  for Lamp in aLevel['Lights']:
    fLights.append( TheSprite(Size = Lamp['Size'], Pos = Lamp['Pos'], Color=Lamp['Color']) )

  RendedMap['Lights'] = fLights# aLevel['Lights']
  
  return RendedMap

  


#RenderLinks
#MapTiles['R']['Link'] = MapTiles['r']

##for y in range(len(Level['Map'])):
##  for x in range(len(Level['Map'][y])):
##    tile = RenderMap[x, y]
##    if (y > 0) and ((y+1) < len(Level['Map']) ):
##      tileDown = RenderMap[x, y +1]
##    # = Sprt
    



KeyControlsSet1 = {'Keys':
                   {
                     pygame.K_F10: 'Exit',
                     pygame.K_UP: 'UP',
                     pygame.K_DOWN: 'DOWN',
                     pygame.K_LEFT: 'LEFT',
                     pygame.K_RIGHT: 'RIGHT',
                     pygame.K_SPACE: 'JUMP',
                     pygame.K_RETURN: 'ACTION',
                  },
                   
                  'ProfileName':
                     'Default keyboard controls'
                  }


KeyControls = KeyControlsSet1

def Distance(x1, y1, x2, y2):
  Dx = x1 - x2
  Dy = y1 - y2
  return sqrt( Dx*Dx + Dy*Dy )

def PicTransparent(pic, size, color):
  cpic = pic.copy()
  ovl = pygame.Surface(size)
  r, g, b, a = color
  ovl.fill((r, g, b))
  ovl.set_alpha(a)
  cpic.blit(ovl, (0, 0))
  return cpic


def PointLight(Pos, Lights):
  vl = []
  md = 1
  r, g, b, a = (0, 0, 0, 255)
  for Light in Lights:
    LPx, LPy = Light['Pos']
    LS = Light['Size']
    Dist = Distance(Pos[0], Pos[1], LPx, LPy)
    md *= LS
    if Dist <= LS:
      vl.append([Dist, Light['Color'], LS])
      
  for Light in vl:
    k = (Light[2] -Light[0])* (md / Light[2])
    lr, lg, lb = Light[1]
    r += lr  * k
    g += lg  * k
    b += lb  * k
    a += 255 * k
      
  r = r/md
  g = g/md
  b = b/md
  a = a/md
  return (r, g, b, 255-a)




def Main():
  tt = PGTicks()
  fps = 0
  surface = pygame.display.get_surface()

  surface.fill((7, 7, 7))

  PG_KD = pygame.KEYDOWN
  PG_MD = pygame.MOUSEBUTTONDOWN
  PG_MU = pygame.MOUSEBUTTONUP
  PG_MM = pygame.MOUSEMOTION


  aLevelName = AllLevels['Start']
  fRendedMap = RenderMap( AllLevels[aLevelName] )
  
  fMapSizeY = fRendedMap['Size'][0]
  fMapSizeX = fRendedMap['Size'][1]
  Lights = fRendedMap['Lights']

  Bulb = TheSprite(Size=6, Pos =(1, 1), Color=(91,  0, 91))
  Lights.append(Bulb)

  running = True
  while running:
    for ev in pygame.event.get():
      EVT = ev.type
      if EVT == pygame.QUIT:
        pygame.display.set_caption(" QUIT ")
        running = False;
          
      if EVT == PG_KD:
        pygame.display.set_caption(pygame.key.name( ev.key ))
        if ev.key in KeyControls['Keys']:
          KAction = KeyControls['Keys'][ev.key]
          if 'Exit' == KAction:
            running = False
            continue

      if (EVT == PG_MD) and (ev.button == 1):
        x,y = ev.pos;
        x = int(x/ThSize)
        y = int(y/ThSize)
        pygame.display.set_caption(str((x,y)) )
        try:
          tile = fRendedMap[x, y]
        except:
          continue
        
        if not tile is None:
          tile.Action('Click', None, ev.button )
          #LightsCoords.append(TheSprite( Pos=(x,y), Color = (random.randint(0, 31), random.randint(0, 31), random.randint(0, 31)), Size = random.randint(6, 11)  ))

      if (EVT == PG_MU) and (ev.button == 2):
        pass

      if (EVT == PG_MM) : #and (ev.buttons == (1, 0, 0)
        x, y = ev.pos;
        Bulb['Pos'] = (x/ThSize, y/ThSize)
        #pygame.display.set_caption(str( fLightPos ) )
        
          
    for y in range(fMapSizeY):
      for x in range(fMapSizeX):
        tile = fRendedMap[x, y]
        Srf = None
        if (tile is not None):
          Srf = tile.Surf()
          
        if Srf is not None:
          # This is Lighting cache
          #if 'Alfa' in tile:
          #  A = tile['Alfa']
          #else:
          #  A = PointLight((x,y), Lights) #LightsCoords
          #  tile['Alfa'] = A
          
          # This is lighting
          #A = PointLight((x,y), Lights) #LightsCoords
          #Srf = PicTransparent(Srf, (ThSize, ThSize), A)
          
          surface.blit(Srf, (x*ThSize, y*ThSize))
          
    pygame.display.update()
    #pygame.display.flip()
    
    fps += 1
    #clock.tick(75)
    #pygame.time.wait(100)
    if (PGTicks()-tt) >= 1000:
      print('FPS: ', str( fps ))
      tt = PGTicks()
      fps = 1


  pygame.quit()
  #pygame.time.wait(250)

Main()
