
import sys #for coloring


class PropContainer(object):

  def __init__(self, **params):
    self.__PropContainer = dict()
    self.OnPropChange = None
    self.OnPropChanged = None
    #self.OnPropChange = self.CheckPropChange
    self.Setup(**params)

  def Setup(self, **params):
    for k, v in params.items():
      self[k] = v
  
  def __getitem__(self, aName):
    if aName in self.__PropContainer:
      return self.__PropContainer[aName]
    else:
      return -1
  
  def __setitem__(self, aName, aValue):
    if (self.OnPropChange is None) or (self.OnPropChange(aName, aValue)):
      self.__PropContainer[aName] = aValue
      if not self.OnPropChanged is None:
        self.OnPropChanged(aName, aValue)

  def GetList(self, params):
    res = []
    for pn in params:
      res.append(self[pn])
    return res

  def SetList(self, params, values):
    ID = 0
    for pn in params:
      self[pn] = values[ID]
      ID += 1

  def __contains__(self, aName):
    return aName in self.__PropContainer

  def __delitem__(self, aName):
    if aName in self.__PropContainer:
      del self.__PropContainer[aName]
    
  def __dir__(self):
    return self.__PropContainer.keys()
  
  def __repr__(self):
    return str(self.__PropContainer.items())

  def CheckPropChange(self, aName, aValue):
    shell = sys.stdout.shell
    shell.write(self.__class__.__name__, 'BUILTIN')
    shell.write(' changing the "', 'COMMENT')
    shell.write(aName, 'STRING')
    shell.write('"', 'COMMENT')
    shell.write(' - ', 'COMMENT')
    shell.write(str(aValue), 'STRING')
    shell.write("\n")
    return True

  pass

